/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.KafkaModelo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/kafka")
public class KafkaController {
    
    private KafkaTemplate<String, KafkaModelo> kafkaTemplate;
    
    
    @Autowired
    public KafkaController(KafkaTemplate<String, KafkaModelo> kafkaTemplate){
        
        this.kafkaTemplate= kafkaTemplate;
        
    }
    
    public void post(@RequestBody KafkaModelo kafkaModelo){
        kafkaTemplate.send("topico", kafkaModelo);
    }
    
}
